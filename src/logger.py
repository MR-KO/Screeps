from defs import *

__pragma__('noalias', 'name')
__pragma__('noalias', 'undefined')
__pragma__('noalias', 'Infinity')
__pragma__('noalias', 'keys')
__pragma__('noalias', 'get')
__pragma__('noalias', 'set')
__pragma__('noalias', 'type')
__pragma__('noalias', 'update')


# Logging output levels
LOG_LEVEL_ERROR		= 0
LOG_LEVEL_WARNING	= 1
LOG_LEVEL_INFO		= 2
LOG_LEVEL_DEBUG		= 3

LOG_DEBUG			= "Debug"
LOG_INFO			= "Info"
LOG_WARNING			= "Warning"
LOG_ERROR			= "Error"

LOG_LEVELS = [LOG_LEVEL_ERROR, LOG_LEVEL_WARNING, LOG_LEVEL_INFO, LOG_LEVEL_DEBUG]
LOG_LEVELS_READABLE = [LOG_ERROR, LOG_WARNING, LOG_INFO, LOG_DEBUG]


class log(object):
	"""
	Very basic logging system that uses printf...
	"""
	level = LOG_LEVEL_DEBUG

	def init(level):
		log.setLevel(level)

	def setLevel(level):
		log.level = level

	def getLogPrefix():
		# TODO: Figure out how to add time...
		# date = __new__(Date())
		# dateString = "{D}{M}{Y} {h}:{m}:{s}".format(
		# 	D=date.GetDate(),
		# 	M=date.GetMonth() + 1,
		# 	Y=date.GetFullYear(),
		# 	h=date.GetHours(),
		# 	m=date.GetMinutes(),
		# 	s=date.GetSeconds())
		# return "{} [{}]: ".format(dateString, LOG_LEVELS_READABLE[log.level])

		# For now, use game tick instead...
		return "{} [{}]: ".format(Game.time, LOG_LEVELS_READABLE[log.level])

	def debug(message):
		# Only print if log level is sufficiently high
		if log.level >= LOG_LEVEL_DEBUG:
			print("{}: {}".format(log.getLogPrefix(), message))

	def info(message):
		# Only print if log level is sufficiently high
		if log.level >= LOG_LEVEL_INFO:
			print("{}: {}".format(log.getLogPrefix(), message))

	def warn(message):
		# Only print if log level is sufficiently high
		if log.level >= LOG_LEVEL_WARNING:
			print("{}: {}".format(log.getLogPrefix(), message))

	def error(message):
		# Only print if log level is sufficiently high
		if log.level >= LOG_LEVEL_ERROR:
			print("{}: {}".format(log.getLogPrefix(), message))


