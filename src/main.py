import config
import creeps

from logger import log

from harvester import Harvester
from builder import Builder
from upgrader import Upgrader

# defs is a package which claims to export all constants and some JavaScript
# objects, but in reality does nothing. This is useful mainly when using an
# editor like PyCharm, so that it 'knows' that things like Object, Creep, Game,
# etc. do exist.
from defs import *

# These are currently required for Transcrypt in order to use the following
# names in JavaScript. Without the 'noalias' pragma, each of the following
# would be translated into something like 'py_Infinity' or 'py_keys' in the
# output file.
__pragma__('noalias', 'name')
__pragma__('noalias', 'undefined')
__pragma__('noalias', 'Infinity')
__pragma__('noalias', 'keys')
__pragma__('noalias', 'get')
__pragma__('noalias', 'set')
__pragma__('noalias', 'type')
__pragma__('noalias', 'update')


# Game notes:
# 	- Creep lifetime: 1500 ticks.
# 	- Current game tick: Game.time .
# 	- Standard spawn can only spawn creeps with a max cost of 300 energy units.
# 	- Each extension adds 50 energy units to that max (required energy must be
# 		present in each spawn and extension).
# 	- Spawns "generate" 1 energy unit per game tick.


# 	- "Game" object and "Memory" object. Available player memory is 2 MB...
# 	- Limited CPU time based on game plan (aka, paying etc): Game.cpuLimit (
# 		current CPU time used via Game.getUsedCpu).
# 	- "Memory" object can store information in JSON via the JSON.stringify and
# 		JSON.parse methods. CPU time for these methods counts too... (You could
# 		write your own methods and use the "RawMemory" object instead).


#	- Game Control Level (GCL), sum of all controller levels you own
#		- Increases CPU limit from an initial 30 CPU to 300 max, with 10 CPU
#			gained per GCL level.
#		- For each GCL level, you can control another room.
#		- Available structures per RCL
#			RCL	Energy to upgrade	Downgrade timer		Structures
#			0	—					neutral				Roads, 5 Containers
#			1	200					20000 ticks			Roads, 5 Containers, 1 Spawn
#			2	45,000				5000 ticks			Roads, 5 Containers, 1 Spawn, 5 Extensions (50 capacity), Ramparts (300K max hits), Walls
#			3	135,000				10000 ticks			Roads, 5 Containers, 1 Spawn, 10 Extensions (50 capacity), Ramparts (1M max hits), Walls, 1 Tower
#			4	405,000				20000 ticks			Roads, 5 Containers, 1 Spawn, 20 Extensions (50 capacity), Ramparts (3M max hits), Walls, 1 Tower, Storage
#			5	1,215,000			40000 ticks			Roads, 5 Containers, 1 Spawn, 30 Extensions (50 capacity), Ramparts (10M max hits), Walls, 2 Towers, Storage, 2 Links
#			6	3,645,000			60000 ticks			Roads, 5 Containers, 1 Spawn, 40 Extensions (50 capacity), Ramparts (30M max hits), Walls, 2 Towers, Storage, 3 Links, Extractor, 3 Labs, Terminal
#			7	10,935,000			100000 ticks		Roads, 5 Containers, 2 Spawns, 50 Extensions (100 capacity), Ramparts (100M max hits), Walls, 3 Towers, Storage, 4 Links, Extractor, 6 Labs, Terminal
#			8	—					150000 ticks		Roads, 5 Containers, 3 Spawns, 60 Extensions (200 capacity), Ramparts (300M max hits), Walls, 6 Towers, Storage, 6 Links, Extractor, 10 Labs, Terminal, Observer, Power Spawn


# 	- Creep body parts:
# 		- WORK: 			Harvest energy, construct and repair structures,
#							upgrade controllers.
# 		- MOVE: 			Ability to move. One move part per other body part to
# 							maintain maximum movement speed of 1 square per tick.
# 		- CARRY: 			Ability to transfer energy (only adds weight if
# 							energy is being carried).
# 		- ATTACK: 			Ability of short-range attack.
# 		- RANGED_ATTACK: 	Ability of ranged attack.
# 		- HEAL: 			Ability to heal others
# 		- CLAIM:			Ability to claim territory control
# 		- TOUGH: 			Armor body part.

# 	- First parts take damage first, full damage disables that part.
# 	- Creeps have 100 hitpoints per each bodypart.
# 	- Upgrade Room Controllers to build extra spawns, roads, extensions etc.
#	- Max 50 body parts per creep


#	- Max 3 spawns per room, 1 spawn can contain 300 energy, extensions 50
#		energy each
# 	- Room size: 50x50 cells
# 	- Floor types:
#		- roads, movement cost 1 (deteriorate due to movement and need repair)
#		- plain land, movement cost 2
#		- swamp, movement cost 10
#		- gane wall, movement cost infinity, no one can pass through
#		- build wall, movement cost infinity, others can't pass through unless
#			destroyed, can't build them no closer than 2 squares to the room
#			edges, and they start with 1 hp, can be fortified with repair action.
#		- ramparts, only my creeps can move in them, cannot be attacked in them,
#			but can attack others. Ramparts deteriorate with each game cycle and
#			require repair, also start with 1 hp and can be fortified with repair
# 		- Newbie walls at the room entrances are up for 20k game ticks.
#	Movement costs are per body part (except MOVE), and creeps cannot move when
#		movement costs are > 0. MOVE body parts reduce movement cost by 2. To
#		get maximum move speed of 1 square per tick, you need as many MOVE parts
#		as all the other parts combined. Empty CARRY parts do not contribute to
#		the movement cost





# - Advanced stuff....
#	- Work out a way to efficiently manage access to a energy/mineral resources,
#		to prevent creeps piling up and blocking access...
# 	- Combining creep methods to make creeps do more in 1 tick... However,
# 		useless actions like healing healthy creeps or repairing undamaged
# 		structures blocks out other actions due to priorities.

# 	- Sequence of called commands is irrelevant, only priorities matter.
# 		When the same method is used again, the last call has the priority.

# 	- Don't use JSON (de)stringify for Memory, but instead use RawMemory
#		instead...



def runCreeps():
	"""
	Runs all our creeps in the game, based on their role
	"""

	# Run each creep based on role
	for name in Object.keys(Game.creeps):
		creep = Game.creeps[name]

		if creep.memory.role == creeps.CREEP_TYPE_HARVESTER:
			harvester = Harvester(creep)
			harvester.run()

		elif creep.memory.role == creeps.CREEP_TYPE_BUILDER:
			builder = Builder(creep)
			builder.run()

		elif creep.memory.role == creeps.CREEP_TYPE_UPGRADER:
			upgrader = Upgrader(creep)
			upgrader.run()

		else:
			# Nothing assigned yet, assume its a harvester... (old shitty code)
			log.debug("[{}] Ermahgerd, I haven't been assigned a role yet...".format(creep.name))
			creep.memory.role = creeps.CREEP_TYPE_HARVESTER
			harvester = Harvester(creep)
			harvester.run()


def determineNewCreep():
	"""
	Determines what type of new creep to spawn, returns one of CREEP_TYPES
	"""

	# TODO: Actually do something less brainless...
	return creeps.CREEP_TYPE_HARVESTER

def spawnNewCreep(spawn):
	"""
	Spawns a new creep if its needed...
	"""
	creepType = determineNewCreep()

	if creepType == creeps.CREEP_TYPE_HARVESTER:
		Harvester.createNew(spawn)

def runSpawns():
	# Run each spawn
	for name in Object.keys(Game.spawns):
		spawn = Game.spawns[name]

		if not spawn.spawning:
			# Get the number of our creeps in this room.
			numCreeps = _.sum(Game.creeps, lambda c: c.pos.roomName == spawn.pos.roomName)

			# TODO: Make this spawning shit more intelligent!

			# If there are no creeps, spawn a creep once energy is at 250 or more
			if numCreeps < 0 and spawn.room.energyAvailable >= 250:
				log.debug("Spawning new creep 1!")
				spawnNewCreep(spawn)

			# If there are less than 15 creeps but at least one, wait until all
			# spawns and extensions are full before spawning.
			elif numCreeps < 15 and spawn.room.energyAvailable >= spawn.room.energyCapacityAvailable:
				# If we have more energy, spawn a bigger creep.
				if spawn.room.energyCapacityAvailable >= 350:
					log.debug("Spawning new creep 2!")
					spawnNewCreep(spawn)
				else:
					log.debug("Spawning new creep 3!")
					spawnNewCreep(spawn)

def main():
	"""
	Main game logic loop.
	"""

	log.init(config.LOG_LEVEL)

	creeps.Creep.getCreepCountersFromMemory()

	# Displays the current tick in the upper-left corner of all rooms
	__new__(RoomVisual().text("Game ticks: {}".format(Game.time), 0, 0,
		{'align': 'left', 'font': 0.6}))


	# TODO: Add tower code? Also, convert this from JS to Python
	# var tower = Game.getObjectById('TOWER_ID');

	# if (tower) {
	# 	var closestDamagedStructure = tower.pos.findClosestByRange(FIND_STRUCTURES, {
	# 		filter: (structure) => structure.hits < structure.hitsMax
	# 	});

	# 	if(closestDamagedStructure) {
	# 		tower.repair(closestDamagedStructure);
	# 	}

	# 	var closestHostile = tower.pos.findClosestByRange(FIND_HOSTILE_CREEPS);

	# 	if(closestHostile) {
	# 		tower.attack(closestHostile);
	# 	}
	# }

	runCreeps()
	runSpawns()

	creeps.Creep.saveCreepCountersToMemory()


module.exports.loop = main
