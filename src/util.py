from defs import *

__pragma__('noalias', 'name')
__pragma__('noalias', 'undefined')
__pragma__('noalias', 'Infinity')
__pragma__('noalias', 'keys')
__pragma__('noalias', 'get')
__pragma__('noalias', 'set')
__pragma__('noalias', 'type')
__pragma__('noalias', 'update')


# Keys for config options used for various functions
TARGET 						= "target"
TRANSFER_RESOURCE_TYPE		= "transferResourceType"
EXEC_IF_CLOSE_FUNCTION		= "executeIfCloseFunction"
EXEC_IF_CLOSE_ARGS			= "executeIfCloseArgs"
RESULT_SUCCESS_LIST			= "listOfSuccessResults"
EXEC_ON_SUCCESS_FUNCTION	= "successFunction"
EXEC_ON_FAILURE_FUNCTION	= "failureFunction"

