from defs import *
from util import *
from creeps import *

from logger import log

import math

__pragma__('noalias', 'name')
__pragma__('noalias', 'undefined')
__pragma__('noalias', 'Infinity')
__pragma__('noalias', 'keys')
__pragma__('noalias', 'get')
__pragma__('noalias', 'set')
__pragma__('noalias', 'type')
__pragma__('noalias', 'update')



class Harvester(Creep):
	"""
	Basic Harvester class.
	TODO: Rewrite this class to use way less move and use transporters instead!
	"""
	ENERGY_PER_LEVEL = BODYPART_COST[WORK] + BODYPART_COST[CARRY] + \
		BODYPART_COST[MOVE] + BODYPART_COST[MOVE]

	def __init__(self, creep):
		Creep.__init__(self, creep)


	def calculateHarvesterLevel(energyAvailable):
		[WORK, CARRY, MOVE, MOVE]

		# Formula: BODYPART_COST[WORK] * NUM_WORK + BODYPART_COST[CARRY] * NUM_CARRY
		# 	+ BODYPART_COST[MOVE] * NUM_MOVE
		# So formula: 100 * NUM_WORK + 50 * NUM_CARRY + 50 * NUM_MOVE, where
		# NUM_MOVE = NUM_WORK + NUM_CARRY (to sustain maximum possible move
		# speed), and ideally, NUM_WORK = NUM_CARRY...
		# So: 100 * x + 50 * x + 50 * 2 * x <= energyAvailable
		# So: 250x <= energyAvailable, meaning that x <= energyAvailable / 250
		level = math.floor(energyAvailable / Harvester.ENERGY_PER_LEVEL)
		return level

	def getBodyArray(level):
		level = max(level, 1)
		body = []

		for i in range(0, level):
			body.push(WORK)

		for i in range(0, level):
			body.push(CARRY)

		for i in range(0, level * 2):
			body.push(MOVE)

		# Unfortunately, this doesn't work :(
		# return [WORK] * level + [CARRY] * level + [MOVE] * level * 2
		return body

	def createNew(spawn):
		"""
		Spawns a new harvester creep at the given spawn.
		"""

		# Get current available energy/level, and spawn a maximal harvester creep...
		level = Harvester.calculateHarvesterLevel(spawn.room.energyAvailable)
		body = Harvester.getBodyArray(level)
		creepType = CREEP_TYPE_HARVESTER

		name = Creep.generateName(creepType)
		log.debug("Spawning {} with level {} and body {}".format(name, level, body))
		spawn.createCreep(body, name, {"memory": {"role": creepType}})

		# Apparantly passing the memory dict does noet work, need to do it manually...
		creep = Game.creeps[name]
		creep.memory.role = CREEP_TYPE_HARVESTER


	def checkFillingStatus(self):
		"""
		Checks to see if the creep is full, and if so, removes the saved source.
		If not, we start filling again...
		"""

		# If we're full, stop filling up and remove the saved source
		if self.creep.memory.filling and self.isFull():
			self.creep.memory.filling = False
			self.deleteSource()

		# If we're empty, start filling again and remove the saved target
		elif not self.creep.memory.filling and self.creep.carry.energy <= 0:
			self.creep.memory.filling = True
			self.deleteTarget()

	def isFilling(self):
		"""
		Returns True/False if the creep is currently filling up.
		"""
		return self.creep.memory.filling

	def getSource(self):
		"""
		Returns the saved source for this harvester, or sets and gets a new one.
		"""

		# If we have a saved source, use it
		if self.creep.memory.source:
			source = Game.getObjectById(self.creep.memory.source)
		else:
			# Get a random new source and save it
			# TODO: Keep track of all sources and divide creeps "evenly"
			# 		among them, or some other smart combination, perhaps
			# 		based on distance? Although if _sample is uniformly random,
			# 		that might work as well as a decent load-balancer...
			source = _.sample(self.creep.room.find(FIND_SOURCES))
			self.creep.memory.source = source.id

		return source

	def getTarget(self):
		"""
		Returns the saved target for this harvester, or sets and gets a new one.
		"""

		# If we have a saved target, use it
		if self.creep.memory.target:
			target = Game.getObjectById(self.creep.memory.target)
		else:
			# Get a random new target.
			# TODO: Don't get a random target, but used some fancy priority
			# shit instead...
			# To prevent going to and (trying to) fill up spawns/extensions
			# that are full, check to see if they have room for more energy.
			target = _(self.creep.room.find(FIND_STRUCTURES)) \
				.filter(lambda s: ( \
					(ENERGY_TARGETS_WITH_LIMITS.includes(s.structureType)) \
						and s.energy < s.energyCapacity) \
					or ENERGY_TARGETS_WITHOUT_LIMITS.includes(s.structureType)) \
				.sample()
			self.creep.memory.target = target.id

		return target

	def deleteSource(self):
		"""
		Deletes the stored source from the creeps memory
		"""
		del self.creep.memory.source

	def deleteTarget(self):
		"""
		Deletes the stored target from the creeps memory
		"""
		del self.creep.memory.target

	def run(self):
		"""
		Runs a creep as a generic harvester.
		"""

		self.checkFillingStatus()

		if self.isFilling():
			# Start filling up from the source, or move towards it
			source = self.getSource()

			self.executeIfNearOrMove(source, {
				EXEC_IF_CLOSE_FUNCTION: self.harvest,
				EXEC_IF_CLOSE_ARGS: {
					EXEC_IF_CLOSE_FUNCTION: "self.harvest",
					TARGET: source},
				EXEC_ON_FAILURE_FUNCTION: self.errorFunc})
		else:
			# We're full, so deposit our energy to our target...
			target = self.getTarget()

			# If we are targeting a spawn or extension, transfer energy.
			if target.energyCapacity:
				self.executeIfNearOrMove(target, {
					EXEC_IF_CLOSE_FUNCTION: self.transfer,
					EXEC_IF_CLOSE_ARGS: {
						EXEC_IF_CLOSE_FUNCTION: "self.transfer",
						TARGET: target,
						TRANSFER_RESOURCE_TYPE: RESOURCE_ENERGY},
					RESULT_SUCCESS_LIST: [OK, ERR_FULL],
					EXEC_ON_SUCCESS_FUNCTION: self.deleteTarget,
					EXEC_ON_FAILURE_FUNCTION: self.errorFunc})
			else:
				# Otherwise, use upgradeController on it.
				self.executeIfNearOrMove(target, {
					EXEC_IF_CLOSE_FUNCTION: self.upgradeController,
					EXEC_IF_CLOSE_ARGS: {
						EXEC_IF_CLOSE_FUNCTION: "self.upgradeController",
						TARGET: target},
					RESULT_SUCCESS_LIST: [OK],
					EXEC_ON_FAILURE_FUNCTION: self.errorFunc})

				# Let the creeps get a little bit closer than required to
				# the controller, to make room for other creeps.
				# TODO: Figure out a way to properly manage access to
				# space-constrained resources/targets...
				if not self.creep.pos.inRangeTo(target, 2):
					self.creep.moveTo(target)

