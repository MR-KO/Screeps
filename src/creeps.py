from defs import *
from util import *

# import logging

__pragma__('noalias', 'name')
__pragma__('noalias', 'undefined')
__pragma__('noalias', 'Infinity')
__pragma__('noalias', 'keys')
__pragma__('noalias', 'get')
__pragma__('noalias', 'set')
__pragma__('noalias', 'type')
__pragma__('noalias', 'update')


# Various types of creeps that are used...
CREEP_TYPE_HARVESTER	= "Harvester"
CREEP_TYPE_BUILDER		= "Builder"
CREEP_TYPE_UPGRADER		= "Upgrader"

# Enumeration of all creep types
CREEP_TYPES = [CREEP_TYPE_HARVESTER, CREEP_TYPE_BUILDER, CREEP_TYPE_UPGRADER]

# Various types of targets that can accept energy, with and without limits to
# their capacity
ENERGY_TARGETS_WITHOUT_LIMITS = [STRUCTURE_CONTROLLER]
ENERGY_TARGETS_WITH_LIMITS = [STRUCTURE_EXTENSION, STRUCTURE_SPAWN]



class Creep(object):
	"""
	Basic Creep class that is used as base class for other types of creeps...
	"""
	CREEP_COUNTERS = {
		CREEP_TYPE_HARVESTER: 0,
		CREEP_TYPE_BUILDER: 0,
		CREEP_TYPE_UPGRADER: 0
	}

	def __init__(self, creep):
		self.creep = creep

		# TODO: Retrieve name, age, etc
		self.age = CREEP_LIFE_TIME

	def generateName(creepType):
		"""
		Generates an unique name for this creep
		"""
		name = "{}{}".format(creepType, Creep.CREEP_COUNTERS[creepType])
		Creep.CREEP_COUNTERS[creepType] += 1
		return name

	def getCreepCountersFromMemory():
		"""
		Reads the creep counters from memory into the Creep class, and creates
		it if it's not yet present in memory
		"""

		# If the memory counters aren't created yet, create them...
		if not Memory.counters:
			Memory.counters = Creep.CREEP_COUNTERS

		Creep.CREEP_COUNTERS = Memory.counters

	def saveCreepCountersToMemory():
		"""
		Saves current creep counters to memory
		"""
		Memory.counters = Creep.CREEP_COUNTERS


	def errorFunc(self, args):
		"""
		Generic error handler function, just prints to the console what
		happened...
		"""
		print("[{}] Unknown result from executing args: {}".format(
			self.creep.name, **args))


	def harvest(self, args):
		"""
		Helper function to
		"""

		# logging.debug("[{}] Harvesting target: {}".format(self.creep.name, args[TARGET]))
		return self.creep.harvest(args[TARGET])

	def transfer(self, args):
		"""
		Helper function to harvest...
		"""
		return self.creep.transfer(args[TARGET], args[TRANSFER_RESOURCE_TYPE])

	def upgradeController(self, args):
		"""
		Helper function to harvest...
		"""
		return self.creep.upgradeController(args[TARGET])


	def isFull(self):
		"""
		Returns True/False if the creep cannot carry any more energy
		"""
		return _.sum(self.creep.carry) >= self.creep.carryCapacity


	def isCloseTo(self, target, distance=3):
		"""
		Returns True/False if we are close enough to the given target. If we
		are targeting a spawn or extension, we need to be directly next to it,
		otherwise we can be 3 away. The spawn and extensions have energy
		capacities, and e.g. a controller does not.
		"""
		if target.energyCapacity:
			return self.creep.pos.isNearTo(target)
		else:
			return self.creep.pos.inRangeTo(target, distance)



	def executeIfNearOrMove(self, target, options={}):
		"""
		Returns True if we are close to our target, and False if we needed to
		move to it. Based on the given options, functions with arguments can be
		executed etc... The following options are handled:

			EXEC_IF_CLOSE_FUNCTION		Executes the given function if the creep
										is close to the target.
			EXEC_IF_CLOSE_ARGS			If the previous option was specified,
										optional args will be provided to the
										previous function.

			RESULT_SUCCESS_LIST			A list of all screeps API codes
										considered to be a success result for
										the EXEC_IF_CLOSE_FUNCTION result. If
										omitted, defaults to [OK].

		If EXEC_IF_CLOSE_FUNCTION was specified, then a screeps API result is
		expected to be returned if the following 2 keys are also present:

			EXEC_ON_SUCCESS_FUNCTION	Executed if result in RESULT_SUCCESS_LIST
			EXEC_ON_FAILURE_FUNCTION	Executed otherwise
		"""

		# If we're near the source, harvest it - otherwise, move to it.
		if self.isCloseTo(target):
			if EXEC_IF_CLOSE_FUNCTION in options:
				executeFunc = options[EXEC_IF_CLOSE_FUNCTION]

				# Execute the specified function, with or without arguments
				if EXEC_IF_CLOSE_ARGS in options:
					args = options[EXEC_IF_CLOSE_ARGS]
					result = executeFunc(**args)
				else:
					result = executeFunc()

				# If specified, get the list of success codes... Unfortunately,
				# Transcrypt does not support the .get method >:(
				successCodes = options[RESULT_SUCCESS_LIST] or [OK]

				# Execute success and failure functions with the same arguments
				if EXEC_ON_SUCCESS_FUNCTION in options and result in successCodes:
					successFunc = options[EXEC_ON_SUCCESS_FUNCTION]
					successFunc(**args)

				if EXEC_ON_FAILURE_FUNCTION in options and result not in successCodes:
					errorFunc = options[EXEC_ON_FAILURE_FUNCTION]
					errorFunc(**args)
		else:
			self.creep.moveTo(target)

