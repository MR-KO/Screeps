from defs import *
from util import *
from creeps import *

__pragma__('noalias', 'name')
__pragma__('noalias', 'undefined')
__pragma__('noalias', 'Infinity')
__pragma__('noalias', 'keys')
__pragma__('noalias', 'get')
__pragma__('noalias', 'set')
__pragma__('noalias', 'type')
__pragma__('noalias', 'update')



class Upgrader(Creep):
	"""
	Basic Upgrader class.
	"""
	harvesterLambda = lambda source: self.creep.harvest(source)

	def __init__(self, creep):
		Creep.__init__(self, creep)

	def checkFillingStatus(self):
		"""
		Checks to see if the creep is full, and if so, removes the saved source.
		If not, we start filling again...
		"""

		# If we're full, stop filling up and remove the saved source
		if self.creep.memory.filling and self.isFull():
			self.creep.memory.filling = False
			self.deleteSource()

		# If we're empty, start filling again and remove the saved target
		elif not self.creep.memory.filling and self.creep.carry.energy <= 0:
			self.creep.memory.filling = True
			self.deleteTarget()

	def isFilling(self):
		"""
		Returns True/False if the creep is currently filling up.
		"""
		return self.creep.memory.filling

	def getSource(self):
		"""
		Returns the saved source for this harvester, or sets and gets a new one.
		"""

		# If we have a saved source, use it
		if self.creep.memory.source:
			source = Game.getObjectById(self.creep.memory.source)
		else:
			# Get a random new source and save it
			# TODO: Keep track of all sources and divide creeps "evenly"
			# 		among them, or some other smart combination, perhaps
			# 		based on distance? Although if _sample is uniformly random,
			# 		that might work as well as a decent load-balancer...
			source = _.sample(self.creep.room.find(FIND_SOURCES))
			self.creep.memory.source = source.id

		return source

	def getTarget(self):
		"""
		Returns the saved target for this harvester, or sets and gets a new one.
		"""

		# If we have a saved target, use it
		if self.creep.memory.target:
			target = Game.getObjectById(self.creep.memory.target)
		else:
			# Get a random new target.
			# TODO: Don't get a random target, but used some fancy priority
			# shit instead...
			# To prevent going to and (trying to) fill up spawns/extensions
			# that are full, check to see if they have room for more energy.
			target = _(self.creep.room.find(FIND_STRUCTURES)) \
				.filter(lambda s: ( \
					(ENERGY_TARGETS_WITH_LIMITS.includes(s.structureType)) \
						and s.energy < s.energyCapacity) \
					or ENERGY_TARGETS_WITHOUT_LIMITS.includes(s.structureType)) \
				.sample()
			self.creep.memory.target = target.id

		return target

	def deleteSource(self):
		"""
		Deletes the stored source from the creeps memory
		"""
		del self.creep.memory.source

	def deleteTarget(self):
		"""
		Deletes the stored target from the creeps memory
		"""
		del self.creep.memory.target

	def run(self):
		"""
		Runs a creep as a generic harvester.
		"""

		self.checkFillingStatus()

		if self.isFilling():
			# Start filling up from the source, or move towards it
			source = self.getSource()

			self.executeIfNearOrMove(source, {
				EXEC_IF_CLOSE_FUNCTION: self.harvest,
				EXEC_IF_CLOSE_ARGS: {
					EXEC_IF_CLOSE_FUNCTION: "self.harvest",
					TARGET: source},
				EXEC_ON_FAILURE_FUNCTION: self.errorFunc})
		else:
			# We're full, so deposit our energy to our target...
			target = self.getTarget()

			# If we are targeting a spawn or extension, transfer energy.
			if target.energyCapacity:
				self.executeIfNearOrMove(target, {
					EXEC_IF_CLOSE_FUNCTION: self.transfer,
					EXEC_IF_CLOSE_ARGS: {
						EXEC_IF_CLOSE_FUNCTION: "self.transfer",
						TARGET: target,
						TRANSFER_RESOURCE_TYPE: RESOURCE_ENERGY},
					RESULT_SUCCESS_LIST: [OK, ERR_FULL],
					EXEC_ON_SUCCESS_FUNCTION: self.deleteTarget,
					EXEC_ON_FAILURE_FUNCTION: self.errorFunc})
			else:
				# Otherwise, use upgradeController on it.
				self.executeIfNearOrMove(target, {
					EXEC_IF_CLOSE_FUNCTION: self.upgradeController,
					EXEC_IF_CLOSE_ARGS: {
						EXEC_IF_CLOSE_FUNCTION: "self.upgradeController",
						TARGET: target},
					RESULT_SUCCESS_LIST: [OK],
					EXEC_ON_FAILURE_FUNCTION: self.errorFunc})

				# Let the creeps get a little bit closer than required to
				# the controller, to make room for other creeps.
				# TODO: Figure out a way to properly manage access to
				# space-constrained resources/targets...
				if not self.creep.pos.inRangeTo(target, 2):
					self.creep.moveTo(target)



def run_harvester(creep):
	"""
	Runs a creep as a generic harvester.
	:param creep: The creep to run
	TODO: Keep track of all sources and divide creeps "evenly" among them, or some other smart combination based on distance?
	"""

	# If we're full, stop filling up and remove the saved source
	if creep.memory.filling and _.sum(creep.carry) >= creep.carryCapacity:
		creep.memory.filling = False
		del creep.memory.source

	# If we're empty, start filling again and remove the saved target
	elif not creep.memory.filling and creep.carry.energy <= 0:
		creep.memory.filling = True
		del creep.memory.target

	if creep.memory.filling:
		# If we have a saved source, use it
		if creep.memory.source:
			source = Game.getObjectById(creep.memory.source)
		else:
			# Get a random new source and save it
			source = _.sample(creep.room.find(FIND_SOURCES))
			creep.memory.source = source.id

		# If we're near the source, harvest it - otherwise, move to it.
		if creep.pos.isNearTo(source):
			result = creep.harvest(source)
			if result != OK:
				print("[{}] Unknown result from creep.harvest({}): {}".format(creep.name, source, result))
		else:
			creep.moveTo(source)
	else:
		# If we have a saved target, use it
		if creep.memory.target:
			target = Game.getObjectById(creep.memory.target)
		else:
			# Get a random new target.
			target = _(creep.room.find(FIND_STRUCTURES)) \
				.filter(lambda s: ((s.structureType == STRUCTURE_SPAWN or s.structureType == STRUCTURE_EXTENSION)
					and s.energy < s.energyCapacity) or s.structureType == STRUCTURE_CONTROLLER) \
				.sample()
			creep.memory.target = target.id

		# If we are targeting a spawn or extension, we need to be directly next to it - otherwise, we can be 3 away.
		if target.energyCapacity:
			is_close = creep.pos.isNearTo(target)
		else:
			is_close = creep.pos.inRangeTo(target, 3)

		if is_close:
			# If we are targeting a spawn or extension, transfer energy. Otherwise, use upgradeController on it.
			if target.energyCapacity:
				result = creep.transfer(target, RESOURCE_ENERGY)

				if result == OK or result == ERR_FULL:
					del creep.memory.target
				else:
					print("[{}] Unknown result from creep.transfer({}, {}): {}".format(
						creep.name, target, RESOURCE_ENERGY, result))
			else:
				result = creep.upgradeController(target)

				if result != OK:
					print("[{}] Unknown result from creep.upgradeController({}): {}".format(
						creep.name, target, result))

				# Let the creeps get a little bit closer than required to the controller, to make room for other creeps.
				if not creep.pos.inRangeTo(target, 2):
					creep.moveTo(target)
		else:
			creep.moveTo(target)
